
## create a virtual environment
'python3 -m venv .venv '   

## activate the virtual environment
'source .venv/bin/activate'

## exit the virtual environment
'deactivate'

## install packages in the venv

make sure you have a requirements.txt file that has package requirements in it with their specific version

'pip3 install -r requirements.txt'