import json
import requests

base_url = 'http://api.postcodes.io/postcodes/'

json_post_codes = {"postcodes" : ["OX49 5NU", "M32 0JG", "NE30 1DP", "E14PX", "sw1a1aa"]}

# print(requests.post(url=base_url, data=json_post_codes))

request_response = requests.post(data=json_post_codes, url=base_url)

request_response_dict = request_response.json()

def hard_coded_practice():
    
    print('\nLocation 1: ')
    print('Postcode:', request_response_dict['result'][0]['result']['postcode'])

    print('Longitude:', request_response_dict['result'][0]['result']['longitude'])

    print('Latitude:', request_response_dict['result'][0]['result']['latitude'])


    print('\nLocation 2: ')
    print('Postcode:', request_response_dict['result'][1]['result']['postcode'])

    print('Longitude:', request_response_dict['result'][1]['result']['longitude'])

    print('Latitude:', request_response_dict['result'][1]['result']['latitude'])


    print('\nLocation 3:')
    print('Postcode:', request_response_dict['result'][2]['result']['postcode'])

    print('Longitude:', request_response_dict['result'][2]['result']['longitude'])

    print('Latitude:', request_response_dict['result'][2]['result']['latitude'])


    print(len(request_response_dict['result']))
    return None

def get_postcode_info():

    try:
        count = 0
        results = request_response_dict['result']
        number_of_results = len(request_response_dict['result'])

        while count < number_of_results:

            Postcode = results[count]['result']['postcode']
            print(f'\nPostcode: {Postcode} ')    

            Longitude = results[count]['result']['longitude']
            print(f'Longitude: {Longitude} ')

            Latitude = results[count]['result']['latitude']
            print(f'Latitude: {Latitude} ')

            count += 1
    except KeyError:
        print('no postcodes were searched')
    
    return None

get_postcode_info()
